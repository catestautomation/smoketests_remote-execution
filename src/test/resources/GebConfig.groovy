import GroovyAutomation.FunctionalLibrary

cacheDriver = false


environments {

    chrome {
        driver = {return FunctionalLibrary.GetDriver("chrome")}
    }

    firefox {
        driver ={return FunctionalLibrary.GetDriver("firefox")}
    }

    ie {
        driver ={return FunctionalLibrary.GetDriver("ie")}
    }

    phantomjs {
        driver ={return FunctionalLibrary.GetDriver("phantomjs")}
    }
    ipadAir2
            {
                Dictionary<String, String> dic = new Hashtable()
                dic.put("device", "iPad Air 2")
                dic.put("os", "ios")
                dic.put("browserstack.user", "sutherson2")
                dic.put("browserstack.key", "mKHznU2DitYiDytd8GDG")

                driver = {
                    return FunctionalLibrary.GetDriver("Remote", dic, "http://hub-cloud.browserstack.com/wd/hub/")
                }
            }

    galaxyTab4
            {
                Dictionary<String, String> dic = new Hashtable()
                dic.put("device", "Galaxy Tab 4 10.1")
                dic.put("os", "Android")
                dic.put("browserstack.user", "sutherson2")
                dic.put("browserstack.key", "mKHznU2DitYiDytd8GDG")

                driver = {
                    return FunctionalLibrary.GetDriver("Remote", dic, "http://hub-cloud.browserstack.com/wd/hub/")
                }
            }

}
