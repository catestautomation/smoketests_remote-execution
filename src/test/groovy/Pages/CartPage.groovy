package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary


class CartPage extends Page{

    // static url = "cart/viewcart"

    static at = {

        cartTable.present
    }

    static content = {


        //headers and Containers
        cartContent { $("div.view-cart-content") }
        productRow { $("tr.msax-BorderTop1").findAll()}
        applyPromoCode { $("div.msax-PromotionCodeStatus").find("span")}

            //Lables
        maxTotalBasedOnQuantity{ $("div.msax-LineTotal").first().text().replace('$', "").replace(",", "").replace(".00",".0").toDouble()}
        totalShippingCharge{$("tr.msax-EstHomeDelivery").first()}
        shippingCharge{totalShippingCharge.$("span").last()}
        homeDelivery{totalShippingCharge.$("span").getAt(1).text().replace('$', "").toDouble()}
        updateZipCode{$("button#UpdateZipcodeBtn")}
        zipCodeTextBox{$("input#zipCodeTextbox")}
        updateZipCodeButton{$("button.msax-Delete16.msax-WithText2.button.tiny", text: "Update")}
        zipCodeAlertMessage{$("div.msax-ErrorPanel.msax-DisplayNone.msax-Info").find("span", text: "You have changed the delivery zip-code, price(s) in your cart may have changed.")}
        kitChildProduct{$("div.msax-PaddingLeft250.msax-KitChild")}
        estimatedTotal{$("tr.msax-Total").find("span").last().text().replace('$', "").replace(",", "").toDouble()}
        promoCodePopUp{$("div#toPopup1")}
        promoCodeValidMessage{$("div.msax-Promocodeitem")}
        promoCodeRemoveLink{promoCodeValidMessage.$("button", text: "(Remove)")}
        editPromoCodeLink {$("div.msax-PromotionCodeStatus").find("span")}
        errorMessage{$("label#lblError1")}
        promoCodeInvalidMessage{$("label", text: "This promotion code is invalid.")}
        promoCodeNotAppliedMessage{$("label",text: "Promotion Code is not applied to any item in cart.")}
        promoCodeError{$("label",text:"Promotion Code is already applied.")}
        promoCodeSpecificError{$("label",text: "The best promotion or sales offer has already been applied or they cannot be combined.")}
        promoCodeHighestError{$("label", text: "One of the promotion or sales offers was removed because these offers cannot be combined.")}
        promoCodeFutureErrorMessage{$("label#lblError1").text()}
        moveToWishList{$("button.msax-Delete16.msax-WithText", text: "Move to Wish List").first()}


        fpoBannerTop{$("div.fpo-banner").first()}
        fpoBannerBottom{$("div.fpo-banner").last()}

        subTotal{$("tr.msax-SubTotal").find("span").getAt(1).text().replace('$', "").replace(",", "").toDouble()}

        productRowRecyclingFee{productRow.get(0).$("a#StateRecyclingFee")}
        productRowRecyclingFeeText{productRowRecyclingFee.$("span", text: "State Recycling Fee:")}
        productRowRecyclingFeeCharge{productRow.get(0).$("span.dollars").first().text().replace('$', "").toDouble()}

        recyclingFeePopUp{$("div#stateRecyclingFee")}
        recyclingFeePopUpCloseButton{$("a.lightboxCloseBtn.button.small.closeButton.right").last()}
        recyclingFeePopUpXClose{$("a.close-reveal-modal").last()}

        estimatedRecyclingFee{$("tr.msax-EstHomeDelivery").last()}
        estimatedRecyclingFeeParent{estimatedRecyclingFee.$("td").first()}
        estimatedRecyclingFeeText{estimatedRecyclingFeeParent.$("span", text: "Est. State Recycling Fee")}
        estimatedRecyclingFeeChargeParent{estimatedRecyclingFee.$("td").last()}
        estimatedRecyclingFeeCharge{estimatedRecyclingFeeChargeParent.$("span.dollars").text().replace('$', "").replace(".00", "").toDouble()}


        //Table
        cartTable{$("table.msax-ItemsGrid")}
        actualPrice { $("div.msax-ItemPriceData").getAt(0).text().replace('$', "").replace(",", "").toDouble() }
        youSave {$("div.msax-LineDiscount.msax-HighlightColor").first().find("span").getAt(1).text().replace('$', "").toDouble()}

        //Buttons
        proceedToCheckoutAtCart { $("button.msax-CheckoutButton.msax-PaddingLeft20").getAt(0) }
        continueShoppingAtCart { $("button.msax-ContinueShoppingButton") }
        applyPromoCodeButton { $("div.msax-PromotionCodeStatus").find("button.msax-Plus16")}
//        applyPromoCodeLightBox { $("button.msax-ApplyPromotionCode") }
        applyPromoCodeLightBox{$("button", text: "Apply").last()}
        closeLightBox { $("div.close") }

        //Text
        enterPromoCodeLightBox { $("input#applyPromotionCodeID") }

        rsaIndicatorOption{$("div#rsaIndicatorView")}
//        rsaIndicatorTitle{rsaIndicatorOption.$("label#rsaIndicatorTitle").find("span", text: "Did a Sales Associate Help You?")} //QA2
        rsaIndicatorTitle{rsaIndicatorOption.$("label#rsaIndicatorTitle").find("span", text: "Did a HomeStore Sales Associate Help You?")} //Stg & CT4
        rsaIndicatorBlock{$("div#rsaIndicatorBlock")}
        rsaIndicatorField{rsaIndicatorBlock.$("input")}
        rsaIndicatorApplyButton{rsaIndicatorBlock.$("button", text: "Apply Name")}// stg
//        rsaIndicatorApplyButton{rsaIndicatorBlock.$("button", text: "Apply")} //CT2

        editRsaIndicatorBlock{$("div#showRsaIndicatorBlock")}
        rsaIndicatorText{editRsaIndicatorBlock.$("span.rsaIndicatorText")}
        editRsaIndicator{editRsaIndicatorBlock.$("button",text: "(edit)")}
        removeRsaIndicator{editRsaIndicatorBlock.$("button",text: "(remove)")}

        promoCodePopUpHeader{promoCodePopUp.$("div#popup_content").find("span",text:"Apply Promo Code")}
        promoCodeDuplicateParent{$("div#divError1")}
        promoCodeDuplicateMessage{promoCodeDuplicateParent.$("label", text: "Promotion Code is already applied.")}
        enteredPromoCodeText{promoCodeValidMessage.$("span.msax-PromotionCodeText")}

        price { $("div.msax-ItemPriceData").first().text() }
        shippingDetails{$("td.msax-ShippingInfo").find("span").findAll()}

        addToWishListPopUp {$ ("div#addToWishList")}
        selectWishListsDropDown  {addToWishListPopUp.$("select#DdlWishLists")}
        createNewWishListButton { $ ("span", text: "CREATE NEW WISH LIST")}
        newWishListNameField { $ ("input#newWishListName")}
        createWishListSuccessPopUp { $ ("div#createWhishListSuccess")}
        wishListButtons { $ ("div.forgotPassbuttons")}
        createWishListButton  {wishListButtons.$ ("a", text: "CREATE")}
        createWishListSuccessPopUpClose {createWishListSuccessPopUp.children().last()}
        addToWishListPopUpMouseOver {$("div.overlay__container")}
        selectWishListsDropDownMouseOver {addToWishListPopUpMouseOver.$("select.wish-list-overlay__select")}
        newWishListNameFieldMouseOver{$("input#wish-list-name")}
        wishListButtonsMouseOver{$("div.create-wish-list-overlay__actions")}
        createWishListButtonMouseOver {wishListButtonsMouseOver.$("button").last()}

        continueShoppingButton { $ ("a#WishListContinueShopping")}

        shoppingCartEmptyText{$("h3", text: "Your Shopping Cart Is Empty")}

        productRemoveLink{$("div.msax-ProductQuickLink").find("button", text:"Remove").first()}

    }

    def enterRsaIndicatorAtCartPage(String rsaNumber){
        FunctionalLibrary.SetText(rsaIndicatorField, rsaNumber)
        FunctionalLibrary.Focus(rsaIndicatorApplyButton)
        rsaIndicatorApplyButton.click()
        FunctionalLibrary.WaitForPageLoad(10)
//        FunctionalLibrary.Click(rsaIndicatorApplyButton)
    }

    def enterPromoCodeAtCartForAssert(String promoCode) {
//        interact { moveToElement(applyPromoCodeButton) }
        FunctionalLibrary.Focus(applyPromoCodeButton)
        FunctionalLibrary.Click(applyPromoCodeButton,20,true)
//        FunctionalLibrary.SetText( currentBrowser.at(CartPage).enterPromoCodeLightBox,promocode)
        enterPromoCodeLightBox.value(promoCode)
//        FunctionalLibrary.SetText(enterPromoCodeLightBox,promoCode)
        FunctionalLibrary.Click(applyPromoCodeLightBox,20)
    }

    def createAnotherWishListAtAddToWishListPopUp(String anotherWishListName, IsSearchProduct = false) {

        if(IsSearchProduct == false) {
            selectWishListsDropDownMouseOver.click()
            selectWishListsDropDownMouseOver.find("option", text: "Create Another Wish List").click()
            Thread.sleep(20000)
            newWishListNameFieldMouseOver.value(anotherWishListName)
            createWishListButtonMouseOver.click()
            Thread.sleep(20000)
        } else {
            selectWishListsDropDown.click()
            selectWishListsDropDown.find("option", text: "CREATE ANOTHER WISH LIST").click()
            Thread.sleep(20000)
            newWishListNameField.value(anotherWishListName)
            createWishListButton.click()
            Thread.sleep(20000)
        }
    }


    def createNewWishList(String wishListName) {
        FunctionalLibrary.Click(createNewWishListButton)
        UUID uuid = UUID.randomUUID();
        wishListName = wishListName + uuid.toString().substring(0, 6)
        FunctionalLibrary.SetText(newWishListNameField, wishListName)
        FunctionalLibrary.Click(createWishListButton)
        FunctionalLibrary.Click(createWishListSuccessPopUpClose)
    }


}
