package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary


class LoginPage extends Page {
    static url="/loginpage"
    static at = {
        signinForm.displayed
    }

    static content = {

        //inputbox
        userName { $("input#LoginControl_TbxEmailLogin") }
        passWord { $("input#LoginControl_TbxPassword") }
        signinForm{$("#SignIn")}
        signInTitle {signinForm.$("h2").text()}
        createAccountForm{$("div#CreateAccount")}
        createAccountTitle{createAccountForm.$("h2").text()}
        checkOutTitle {$("h1").text()}
        guestCheckOutTitle{$("h4").last().text()}

        //Text Boxes
        firstName{$("input#scphrightsection_0_TbxCreateFirstName")}
        lastName{$("input#scphrightsection_0_TbxCreateLastName")}
        email{$("input#scphrightsection_0_TbxCreateEmail")}
        confirmEmail{$("input#scphrightsection_0_TbxCreateConfirmEmail")}
        createPassword{$("input#scphrightsection_0_TbxCreatePassword")}
        confirmPassword{$("input#scphrightsection_0_TbxCreateConfirmPassword")}
        //Check Box
        ageConfirmCheckBox{$("input#scphrightsection_0_ChbxUserAgeConfirm")}

        //Error Message
        firstNameErrorMessage{firstName.parent().find("span", text: "Please enter your first name.")}
        lastNameErrorMessage{lastName.parent().find("span", text: "Please enter your last name.")}
        emailErrorMessage{email.parent().find("span", text: "Please enter your email.")}
        confirmEmailErrorMessage{confirmEmail.parent().find("span", text: "The emails provided do not match.")}
        createPasswordErrorMessage{createPassword.parent().find("span", text: "Please enter a password of at least 8 characters.")}
        confirmPasswordErrorMessage{confirmPassword.parent().find("span", text: "The passwords provided do not match.")}

        createPasswordHint{createPassword.parent().find("span", text: "Please enter a password of at least 8 characters.")}

        //buttons
        loginButton{ $("#LoginControl_SignInButton") }
        guestButton { $(".large-6.column.light-border").find(".button.small").last() }
        createAccountButton{$("a#scphrightsection_0_CreateAccountButton")}
        forgotPassword{$("a#forgotPasswordLink")}
    }

    def userLogin(String username, String password) {
        FunctionalLibrary.SetText(userName, username)
        FunctionalLibrary.SetText(passWord, password)
        FunctionalLibrary.Focus(loginButton)
//        currentBrowser.at(LoginPage).loginButton.click()
        FunctionalLibrary.Click(loginButton,150,true)
//        FunctionalLibrary.WaitForPageLoad()
    }

    def newUserAccountCreation(String FirstName, String LastName, String Email, String Password) {

        FunctionalLibrary.SetText(firstName, FirstName)
        FunctionalLibrary.SetText(lastName, LastName)
        FunctionalLibrary.SetText(email, Email)
        FunctionalLibrary.SetText(confirmEmail, Email)
        FunctionalLibrary.SetText(createPassword, Password)
        FunctionalLibrary.SetText(confirmPassword, Password)
        ageConfirmCheckBox.value(true)
        FunctionalLibrary.Click(createAccountButton,150,true)
//        currentBrowser.at(LoginPage).createAccountButton.click()
//        FunctionalLibrary.WaitForPageLoad()
    }

}
