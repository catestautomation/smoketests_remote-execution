package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary


class PaypalPage extends Page {

    static at = {
        body.present
    }

    static content = {
        body{$("body")}
        payPalEmail{$("input#email")}
        payPalPassword{$("input#password")}
        payPalLoginButton{$("button#btnLogin")}
        payPalContinueButton{$("input#confirmButtonTop")}
    }

    def payPalCardDetails(String paypalEmail, String paypalPassword) {
        FunctionalLibrary.SetText(payPalEmail, paypalEmail)
        FunctionalLibrary.SetText(payPalPassword, paypalPassword)
        FunctionalLibrary.Click(payPalLoginButton)
        Thread.sleep(20000)
    }
}