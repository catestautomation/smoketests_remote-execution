package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

class Footer extends Page {

    static at = {
        footerParent.present
    }

    static content = {

        footerContents { $("div.row.footerBottom") }
        footerParent { footerContents.$("div.contentCenter") }

        //Links
        aboutUs { footerParent.$("a", text: contains("About Us")) }
        careers { footerParent.$("a", text: contains("Careers")) }
        news { footerParent.$("a", text: contains("News")) }
        contactUs { footerParent.$("a", text: contains("Contact Us")) }
        storeLocation { footerParent.$("a", text: contains("Store Locations")) }

        customerCare { footerParent.$("a", text: contains("Customer Care")) }
        shopping { footerParent.$("a", text: contains("Shopping")) }
        returns { footerParent.$("a", text: contains("Returns")) }
//        deliveryInformation { footerParent.$("a", text: contains("Delivery Information")) }//QA2
        deliveryInformation { footerParent.$("a", text: contains("Shipping & Delivery")) } //Stg
        fAQ { footerParent.$("a", text: contains("FAQ")) }
        careAndCleaning { footerParent.$("a", text: contains("Care & Cleaning")) }
        warrantyInformation { footerParent.$("a", text: contains("Warranty Information")) }
        glossary { footerParent.$("a", text: contains("Glossary")) }

        blog { footerParent.$("a", text: contains("Blog")) }
        ourCatalog { footerParent.$("a", text: contains("Our Catalog")) }
        roomPlanner { footerParent.$("a", text: contains("Room Planner")) }
        hopeToDream { footerParent.$("a", text: contains("Hope To Dream")) }

        orderStatus { footerParent.$("a", text: contains("Order Status")) }
        SignIn { footerParent.$("a", text: contains("Sign In")) }
        forgotPassword { footerParent.$("a", text: contains("Forgot Password")) }

        footerBottom { $("div#listBrands") }
        copyRightsParent { footerBottom.$("div.column.medium-7.large-6.copyright") }
        offersAndDetails { copyRightsParent.$("a", text: contains("Offers & Details")) }
        termsAndCondition { copyRightsParent.$("a", text: contains("Terms & Conditions")) }
        termsOfUse { copyRightsParent.$("a", text: contains("Terms of Use")) }
        privacyPolicy { copyRightsParent.$("a", text: contains("Privacy Policy")) }

        contactUsHelp{$("div.row.toutBlock.short")}
        questionForm{$("fieldset.general.contact-form")}
        questionFormHeading{questionForm.$("p", text: "We would love to hear what you think about our company, our products, our retailers, our Website or anything else that comes to mind. All of your comments and suggestions are appreciated.")}
        contactUsHelpOption5{contactUsHelp.$("a", text: "I need help with something not covered above")}
        contactUsHelpOption5Submit{$("button", text: "Submit")}
        contactUsHelpOption5SubjectError{$("p", text: "Please select the type of comment")}
        contactUsHelpOption5NameError{$("p", text: "Please enter your full name.")}
        contactUsHelpOption5NumberError{$("p", text: "Please enter your phone number.")}
        contactUsHelpOption5EmailError{$("p", text: "Please enter your email address.")}
        contactUsHelpOption5CommentError{$("p", text: "Please enter your comment")}

        subjectDropDownParent{$("select#general-topic")}
        subjectDropDown{subjectDropDownParent.$("option", text: "Please Select Subject")}
        contactUsOption5Name{$("input#general-customer-name")}
        contactUsOption5PhoneNumber{$("input#general-phone-number")}
        contactUsOption5Email{$("input#general-email")}
        contactUsOption5Comments{$("textarea#general-comments")}
        contactUsOption5ConfirmationPopUp{$("div#contact-confirmation")}

        //Option 1 blank data
        contactUsHelpOption1QuestionForm{$("fieldset.product-inquire.contact-form")}
//        contactUsHelpOption1{contactUsHelp.$("a", text: "I need more information about one of your products")}//CT2
        contactUsHelpOption1{contactUsHelp.$("a", text: "I'm having a problem with the website or need more information about a product")} //stg
        contactUsHelpOption1Submit{$("button", text: "Submit")}
        contactUsHelpOption1ProductError{$("p", text: "Please provide the website issue or the product name.")} // STG
//        contactUsHelpOption1ProductError{$("p", text: "Please select a product type.")} //CT2
        contactUsHelpOption1NameError{$("p", text: "Please enter your full name.")}
        contactUsHelpOption1NumberError{$("p", text: "Please enter your phone number.")}
        contactUsHelpOption1EmailError{$("p", text: "Please enter your email address.")}
        contactUsHelpOption1CommentsError{$("p", text: "Please enter your comment.")}

        //Option 1 invalid/valid data
        contactUsHelpOption1ProductType{$("input#product-inquire-furniture-type")}
        contactUsHelpOption1FullName{$("input#product-inquire-customer-name")}
        contactUsHelpOption1PhoneNumber{$("input#product-inquire-phone-number")}
        contactUsHelpOption1Email{$("input#product-inquire-email")}
        contactUsHelpOption1Comments{$("textarea#product-inquire-comments")}

        //Valid Data
        contactUsHelpOption1ContactConfirmationClose{$("button", text: "CLOSE")}
        contactUsHelpOption1ContactConfirmation{$("p", text: "Your message was sent")}

        //Option 2 blank data
        contactUsHelpOption2QuestionForm{$("fieldset.product-issue.contact-form")}
        contactUsHelpOption2{contactUsHelp.$("a", text: "I'm having a problem with a purchased product")}
        contactUsHelpOption2Submit{$("button", text: "Submit")}
        contactUsHelpOption2PurchaseTypeError{$("p", text: "Please select the purchase type")}
        contactUsHelpOption2StoreNameError{$("p", text: "Please enter the store name.")}
        contactUsHelpOption2StoreCityError{$("p", text: "Please enter the city where the store is located.")}
        contactUsHelpOption2MonthError{$("p", text: "Please select a purchase month.")}
        contactUsHelpOption2ModelNumberError{$("p", text: "Please enter the Model Number.")}
        contactUsHelpOption2SerialNumberError{$("p", text: "Please enter the Serial Number.")}
        contactUsHelpOption2FullNameError{$("p", text: "Please enter your full name.")}
        contactUsHelpOption2PhoneNumberError{$("p", text: "Please enter your phone number.")}
        contactUsHelpOption2EmailError{$("p", text: "Please enter your email address.")}
        contactUsHelpOption2CommentsError{$("p", text: "Please enter your comment.")}

        //Option 2 invalid/valid data
        purchaseTypeDropDownParent{$("select#product-issue-purchase-type")}
        contactUsHelpOption2StoreName{$("input#product-issue-store-name")}
        contactUsHelpOption2StoreCity{$("input#product-issue-store-city")}
        contactUsHelpOption2StoreState{$("select#product-issue-store-state")}
        contactUsHelpOption2Month{$("select#product-issue-month")}
        contactUsHelpOption2Year{$("select#product-issue-year")}
        contactUsHelpOption2ModelNumber{$("input#product-issue-model-number")}
        contactUsHelpOption2SerialNumber{$("input#product-issue-serial-number")}
        contactUsHelpOption2FullName{$("input#product-issue-customer-name")}
        contactUsHelpOption2PhoneNumber{$("input#product-issue-phone-number")}
        contactUsHelpOption2Email{$("input#product-issue-email")}
        contactUsHelpOption2Comments{$("textarea#product-issue-comments")}


        //Option 3 Valid Data confirmation pop-up
        contactUsHelpOption2ContactConfirmationClose{$("button", text: "CLOSE")}
        contactUsHelpOption2ContactConfirmation{$("p", text: "Your message was sent")}

        contactUsHelpOption3QuestionForm{$("fieldset.product-issue.contact-form")}
        contactUsHelpOption3{contactUsHelp.$("a", text: "I'm having a problem with a retailer")}
        contactUsHelpOption3Submit{$("button", text: "Submit")}
        contactUsHelpOption3retailerTypeError{$("p", text: "Please enter the type of retailer.")}
        contactUsHelpOption3StoreNameError{$("p", text: "Please enter the store name.")}
        contactUsHelpOption3StoreCityError{$("p", text: "Please enter the city where the store is located")}
        contactUsHelpOption3FullNameError{$("p", text: "Please enter your full name.")}
        contactUsHelpOption3PhoneNumberError{$("p", text: "Please enter your phone number.")}
        contactUsHelpOption3EmailError{$("p", text: "Please enter your email address.")}
        contactUsHelpOption3CommentsError{$("p", text: "Please enter your comment.")}

        //Option 3 invalid/valid data
        retailerTypeDropDownParent{$("select#retailer-retailer-type")}
        contactUsHelpOption3StoreName{$("input#retailer-store-name")}
        contactUsHelpOption3StoreCity{$("input#retailer-store-city")}
        contactUsHelpOption3StoreState{$("select#retailer-store-state")}
        contactUsHelpOption3FullName{$("input#retailer-customer-name")}
        contactUsHelpOption3PhoneNumber{$("input#retailer-phone-number")}
        contactUsHelpOption3Email{$("input#retailer-email")}
        contactUsHelpOption3Comments{$("textarea#retailer-comments")}


        //Option 3 Valid Data confirmation pop-up
        contactUsHelpOption3ContactConfirmationClose{$("button", text: "CLOSE")}
        contactUsHelpOption3ContactConfirmation{$("p", text: "Your message was sent")}

        company {footerParent.find("li").first().text()}
        customerSupport{footerParent.find("li").first().text()}
        getInspired{footerParent.find("li").first().text()}
        myAccount{footerParent.find("li").first().text()}

        footerLinks{footerParent.findAll()}

        // option 5
        contactUsHelpOption4{contactUsHelp.$("a", text: "I received a suspicious email about Ashley Furniture")}
        contactUsHelpOption4Submit{$("button", text: "Submit")}
        contactUsHelpOption4FullName{$("input#email-customer-name")}
        contactUsHelpOption4PhoneNumber{$("input#email-phone-number")}
        contactUsHelpOption4Email{$("input#email-email")}
        contactUsHelpOption4PerpetratorScammersMail{$("input#email-scammer")}
        contactUsHelpOption4emailDetails{$("textarea#email-details")}
        contactUsHelpOption4Comments{$("textarea#email-comments")}
        contactUsHelpOption4checkbox{$("input#email-contact")}

        contactUsHelpOption4FullNameError{$("p", text:"Please enter your full name.")}
        contactUsHelpOption4PhoneNumberError{$("p", text:"Please enter your phone number")}
        contactUsHelpOption4EmailError{$("p",text:"Please enter your email address.")}
        contactUsHelpOption4PerpetratorScammersMailError{$("p", text:"Please enter the Perpetrator\"/\"Scammer�s email.")}
        contactUsHelpOption4emailDetailsError{$("p",text:"Please enter the email details.")}
        contactUsHelpOption4CommentsError{$("p",text:"Please enter your comment")}


        heading{$("div.row.sectionTitle").find(("h1")).text()}
        shopLikeAnInsiderParent{$("div.contentSignIn.clearfix")}
        shopLikeAnInsider{shopLikeAnInsiderParent.$("input#TbxEmailFooter")}
        signUp{shopLikeAnInsiderParent.$("a#signUpButton")}

//        heading{$("div.newsletter-sign-up").find(("h3")).text()}
//        shopLikeAnInsiderParent{$("div.newsletter-sign-up__form")}
//        shopLikeAnInsider{shopLikeAnInsiderParent.$("input")}
//        signUp{shopLikeAnInsiderParent.$("button", text: "Sign Up")}

        overLay{$("div#websiteOverlay")}
        confirmEmailId{$("input#confirmEmailFieldID")}
        firstName{$("input#firstNameFieldID")}
        zipCode{$("input#zipCodeFieldID")}
        ageConfirmCheckbox{$("div.columns.large-12.small-12").find("input#sgnUpChbxAgeConfirm")}
        submitMe{$("input#ctl13_emailOverlaySubmit")}
        closeSuccessOverlay{$("a.small.button.successPopup")}

        contactFormsContainer{$("div#contact-forms").find("fieldset.email.contact-form")}
        collectingEmailAddressHeader{contactFormsContainer.$("p").find("a")}

        helpFulTips{contactFormsContainer.$("a#email-tips")}
        helpFulTipsContainer{$("div#email-tips-content")}
        helpFulTipsHeader{helpFulTipsContainer.find("h3").text()}
        helpFulTipsContents{helpFulTipsContainer.$("li").findAll()}
        helpFulTipsClose{helpFulTipsContainer.find("button", text: "CLOSE")}

        contactConfirmation{$("div#contact-confirmation")}

    }

    def enterSomethingNotCoveredAboveQuestions(String subjectName, String fullName, String phoneNumber, String emailAddress, String comments) {
        subjectDropDownParent.click()
        subjectDropDownParent.find("option", text: subjectName).click()
        FunctionalLibrary.SetText(contactUsOption5Name, fullName )
        FunctionalLibrary.SetText(contactUsOption5PhoneNumber, phoneNumber)
        FunctionalLibrary.SetText(contactUsOption5Email, emailAddress)
        FunctionalLibrary.SetText(contactUsOption5Comments, comments)
        FunctionalLibrary.Click(contactUsHelpOption5Submit)
    }


    def moreInformationAboutOneOfYourProduct(String productType,String fullName, String phoneNumber, String email, String comments) {
        FunctionalLibrary.SetText(contactUsHelpOption1ProductType,productType)
        FunctionalLibrary.SetText(contactUsHelpOption1FullName,fullName)
        FunctionalLibrary.SetText(contactUsHelpOption1PhoneNumber,phoneNumber)
        FunctionalLibrary.SetText(contactUsHelpOption1Email,email)
        FunctionalLibrary.SetText(contactUsHelpOption1Comments,comments)
        FunctionalLibrary.Click(contactUsHelpOption1Submit)
    }

    def problemWithPurchasedProduct(String purchaseType, String storeName, String storeCity, String storeState, String monthOfPurchase, String yearOfPurchase, String modelNumber, String serialNumber, String fullName, String phoneNumber, String email, String comments) {
        purchaseTypeDropDownParent.click()
        purchaseTypeDropDownParent.find("option", text: purchaseType).click()
        if(purchaseType == "In Store") {
            FunctionalLibrary.SetText(contactUsHelpOption2StoreName, storeName)
            FunctionalLibrary.SetText(contactUsHelpOption2StoreCity, storeCity)
            contactUsHelpOption2StoreState.click()
            contactUsHelpOption2StoreState.find("option", text: storeState).click()
        }
        contactUsHelpOption2Month.click()
        contactUsHelpOption2Month.find("option", text: monthOfPurchase).click()
        contactUsHelpOption2Year.click()
        contactUsHelpOption2Year.find("option", text: yearOfPurchase).click()
        FunctionalLibrary.SetText(contactUsHelpOption2ModelNumber,modelNumber)
        FunctionalLibrary.SetText(contactUsHelpOption2SerialNumber,serialNumber)
        FunctionalLibrary.SetText(contactUsHelpOption2FullName,fullName)
        FunctionalLibrary.SetText(contactUsHelpOption2PhoneNumber,phoneNumber)
        FunctionalLibrary.SetText(contactUsHelpOption2Email,email)
        FunctionalLibrary.SetText(contactUsHelpOption2Comments,comments)
        FunctionalLibrary.Click(contactUsHelpOption2Submit)
    }

    def problemWithRetailer(String retailType, String storeName, String storeCity, String storeState, String fullName, String phoneNumber, String email, String comments) {
        retailerTypeDropDownParent.click()
        retailerTypeDropDownParent.find("option", text: retailType).click()
        FunctionalLibrary.SetText(contactUsHelpOption3StoreName, storeName)
        FunctionalLibrary.SetText(contactUsHelpOption3StoreCity, storeCity)
//        contactUsHelpOption3StoreState.click()
//        contactUsHelpOption3StoreState.find("option", text: storeState).click()
        FunctionalLibrary.SetText(contactUsHelpOption3FullName,fullName)
        FunctionalLibrary.SetText(contactUsHelpOption3PhoneNumber,phoneNumber)
        FunctionalLibrary.SetText(contactUsHelpOption3Email,email)
        FunctionalLibrary.SetText(contactUsHelpOption3Comments,comments)
        FunctionalLibrary.Click(contactUsHelpOption3Submit)
    }

    def enterSignUpDetails(String ConfirmEmail,String FirstName,String ZipCode){
        FunctionalLibrary.SetText(confirmEmailId, ConfirmEmail)
        FunctionalLibrary.SetText(firstName, FirstName)
        FunctionalLibrary.SetText(zipCode, ZipCode)
        //ageConfirmCheckbox.value(true)
        FunctionalLibrary.Click(ageConfirmCheckbox)
        FunctionalLibrary.Click((submitMe))
    }
    def receivedSuspiciousEmailAboutAshley(String FullName, String PhoneNumber, String Email, String scammersMail, String FradulentEmail, String Comments){
        FunctionalLibrary.SetText(contactUsHelpOption4FullName,FullName)
        FunctionalLibrary.SetText(contactUsHelpOption4PhoneNumber,PhoneNumber)
        FunctionalLibrary.SetText(contactUsHelpOption4Email,Email)
        FunctionalLibrary.SetText(contactUsHelpOption4PerpetratorScammersMail,scammersMail)
        FunctionalLibrary.SetText(contactUsHelpOption4emailDetails,FradulentEmail)
        FunctionalLibrary.SetText(contactUsHelpOption4Comments,Comments)
        FunctionalLibrary.Click(contactUsHelpOption4Submit)

    }
}

